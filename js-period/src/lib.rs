use std::str::FromStr;

use time_period as tp;
use tp::PDateTime;
use wasm_bindgen::prelude::*;

mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
#[derive(Debug)]
pub struct Period {
    period: tp::Period,
}

impl From<tp::Period> for Period {
    fn from(period: tp::Period) -> Self {
        Self { period }
    }
}

#[wasm_bindgen]
impl Period {
    /// Creates a period with start and end date.
    #[wasm_bindgen(constructor)]
    pub fn new(start_date: js_sys::Date, end_date: js_sys::Date) -> Result<Period, js_sys::Error> {
        Self::from_iso(start_date.to_iso_string(), end_date.to_iso_string())
    }

    pub fn from_iso(
        start_date: js_sys::JsString,
        end_date: js_sys::JsString,
    ) -> Result<Period, js_sys::Error> {
        let period = match tp::Period::from_iso(&start_date.to_string(), &end_date.to_string()) {
            Ok(period) => period,
            Err(error) => return Err(js_sys::Error::new(&format!("{}", error)[..])),
        };

        match period {
            None => Err(js_sys::Error::new("Inexistant period")),
            Some(period) => Ok(Period { period }),
        }
    }

    #[wasm_bindgen(getter)]
    pub fn start(&self) -> js_sys::JsString {
        return js_sys::JsString::from(self.period.start.to_rfc3339());
    }

    #[wasm_bindgen(getter)]
    pub fn end(&self) -> js_sys::JsString {
        return js_sys::JsString::from(self.period.end.to_rfc3339());
    }

    pub fn duration_in_seconds(&self) -> js_sys::Number {
        return js_sys::Number::from(
            i32::try_from(self.period.duration().num_seconds())
                .expect("Number of second is too large for JS Number"),
        );
    }

    pub fn contains(&self, date: js_sys::Date) -> js_sys::Boolean {
        self.period
            .contains(&PDateTime::from_str(&String::from(date.to_iso_string())).unwrap())
            .into()
    }

    pub fn is_before(&self, period: Self) -> js_sys::Boolean {
        self.period.is_before(&period.period).into()
    }

    pub fn is_heading(&self, period: Self) -> js_sys::Boolean {
        self.period.is_heading(&period.period).into()
    }

    pub fn is_included(self, period: Self) -> js_sys::Boolean {
        self.period.is_included(&period.period).into()
    }

    pub fn is_tailing(&self, period: Self) -> js_sys::Boolean {
        self.period.is_tailing(&period.period).into()
    }

    pub fn is_after(&self, period: Self) -> js_sys::Boolean {
        self.period.is_after(&period.period).into()
    }

    pub fn is_strictly_after(self, period: Self) -> js_sys::Boolean {
        self.period.is_strictly_after(&period.period).into()
    }

    pub fn is_intersecting(self, period: Self) -> js_sys::Boolean {
        self.period.is_intersecting(&period.period).into()
    }

    pub fn is_strictly_before(self, period: Self) -> js_sys::Boolean {
        self.period.is_strictly_before(&period.period).into()
    }

    pub fn union(&self, period: Self) -> Option<Period> {
        match self.period.union(&period.period) {
            Some(p) => Some(Self::from(p)),
            None => None,
        }
    }

    pub fn maximize(&self, period: Self) -> Self {
        Self::from(self.period.maximize(&period.period))
    }

    pub fn between(&self, period: Self) -> Option<Period> {
        match self.period.between(&period.period) {
            Some(p) => Some(Self::from(p)),
            None => None,
        }
    }
}
