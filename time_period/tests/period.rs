use time_period::Period;

pub mod references;
use references::{dates, periods};

#[test]
fn create_period() {
    let period = Period::new(dates::start(), dates::end());
    assert_eq!(
        format!("{}", period.expect("period exists")),
        "[2023-03-09T12:00:00+00:00,2023-05-17T12:00:00+00:00)"
    );
}

#[test]
fn create_inexistant_period() {
    let period = Period::new(dates::start(), dates::start());

    assert_eq!(period, None);
}

#[test]
fn test_format() {
    assert_eq!(
        format!("{}", periods::reference()),
        "[2023-03-09T12:00:00+00:00,2023-05-17T12:00:00+00:00)"
    );
}

#[test]
fn equal() {
    let period = Period::new(dates::start(), dates::end());
    let period2 = Period::new(dates::start(), dates::end());
    assert_eq!(period, period2);
}

#[test]
fn create_period_from_string() {
    let period = Period::from_iso("2022-01-01T12:00:00+02:00", "2022-01-02T12:00:00+04:00")
        .unwrap()
        .expect("period exists");

    assert_eq!(
        format!("{}", period),
        "[2022-01-01T10:00:00+00:00,2022-01-02T08:00:00+00:00)"
    );
}

#[test]
fn get_duration() {
    assert_eq!(periods::reference().duration(), chrono::Duration::days(69));
}

#[test]
fn contains() {
    assert_eq!(periods::reference().contains(&dates::d03()), false);
    assert_eq!(periods::reference().contains(&dates::start()), true);
    assert_eq!(periods::reference().contains(&dates::end()), false);
    assert_eq!(periods::reference().contains(&dates::d05()), true);
}
