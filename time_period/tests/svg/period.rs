use super::{svg_translate, SVGCoordX, SVGText, SVGWidth, GAP, HEIGHT, LINE_HEIGHT, SECOND_PER_PX};
use super::{SVGGroup, SVGTest};
use crate::references::periods::PeriodMethodResults;
use crate::references::periods::PeriodMethodTest;
use svg::node::element::{Group, Rectangle};
use time_period::Period;

impl From<Period> for SVGWidth {
    fn from(period: Period) -> Self {
        SVGWidth(period.duration().num_seconds() / SECOND_PER_PX)
    }
}

impl From<PeriodMethodTest<bool>> for SVGTest {
    fn from(method_test: PeriodMethodTest<bool>) -> Self {
        let svg_method_test = SVGTest::new(&method_test.title, super::SVGTestObject::Period)
            .add(SVGGroup::from((&method_test.title, method_test.before)))
            .add(SVGGroup::from((&method_test.title, method_test.head)))
            .add(SVGGroup::from((&method_test.title, method_test.starting)))
            .add(SVGGroup::from((&method_test.title, method_test.same)))
            .add(SVGGroup::from((&method_test.title, method_test.included)))
            .add(SVGGroup::from((&method_test.title, method_test.enclose)))
            .add(SVGGroup::from((&method_test.title, method_test.ending)))
            .add(SVGGroup::from((&method_test.title, method_test.tail)))
            .add(SVGGroup::from((&method_test.title, method_test.after)));
        return svg_method_test;
    }
}

impl From<PeriodMethodTest<Period>> for SVGTest {
    fn from(method_test: PeriodMethodTest<Period>) -> Self {
        let svg_method_test = SVGTest::new(&method_test.title, super::SVGTestObject::Period)
            .add(SVGGroup::from((&method_test.title, method_test.before)))
            .add(SVGGroup::from((&method_test.title, method_test.head)))
            .add(SVGGroup::from((&method_test.title, method_test.starting)))
            .add(SVGGroup::from((&method_test.title, method_test.same)))
            .add(SVGGroup::from((&method_test.title, method_test.included)))
            .add(SVGGroup::from((&method_test.title, method_test.enclose)))
            .add(SVGGroup::from((&method_test.title, method_test.ending)))
            .add(SVGGroup::from((&method_test.title, method_test.tail)))
            .add(SVGGroup::from((&method_test.title, method_test.after)));
        return svg_method_test;
    }
}

impl From<PeriodMethodTest<Option<Period>>> for SVGTest {
    fn from(method_test: PeriodMethodTest<Option<Period>>) -> Self {
        let svg_method_test = SVGTest::new(&method_test.title, super::SVGTestObject::Period)
            .add(SVGGroup::from((&method_test.title, method_test.before)))
            .add(SVGGroup::from((&method_test.title, method_test.head)))
            .add(SVGGroup::from((&method_test.title, method_test.starting)))
            .add(SVGGroup::from((&method_test.title, method_test.same)))
            .add(SVGGroup::from((&method_test.title, method_test.included)))
            .add(SVGGroup::from((&method_test.title, method_test.enclose)))
            .add(SVGGroup::from((&method_test.title, method_test.ending)))
            .add(SVGGroup::from((&method_test.title, method_test.tail)))
            .add(SVGGroup::from((&method_test.title, method_test.after)));
        return svg_method_test;
    }
}

pub struct SVGPeriodGroup(pub Group);

impl SVGPeriodGroup {
    pub fn new(name: String, period: Period) -> Self {
        let width: i64 = SVGWidth::from(period).into();
        let x: i64 = SVGCoordX::from(period.start).into();
        let rectangle = Rectangle::new()
            .set("class", format!("period {}", name))
            .set("width", width)
            .set("height", HEIGHT)
            .set("x", x)
            .set("y", 0);

        let title = SVGText::from(name)
            .0
            .set("class", "period_title")
            .set("y", LINE_HEIGHT / 2 + GAP)
            .set("x", x + width / 2)
            .set("text-anchor", "middle");
        Self(Group::new().add(rectangle).add(title))
    }
}

impl From<(Period, Period)> for SVGGroup {
    fn from((p1, p2): (Period, Period)) -> Self {
        SVGGroup {
            group: Group::new()
                .add(SVGPeriodGroup::new("P1".to_string(), p1).0)
                .add(SVGPeriodGroup::new("P2".to_string(), p2).0),
            line_count: 2,
        }
    }
}

impl From<(&String, PeriodMethodResults<bool>)> for SVGGroup {
    fn from((method, results): (&String, PeriodMethodResults<bool>)) -> Self {
        let result_text = SVGText::from(format!("p1.{0}(p2) = {1}", method, results.result))
            .0
            .set("class", format!("is-{}", results.result))
            .set("transform", svg_translate(&0, &(LINE_HEIGHT * 2)));

        let group = SVGGroup::from((results.period1, results.period2))
            .group
            .add(result_text);
        SVGGroup {
            group,
            line_count: 2,
        }
    }
}

impl From<(&String, PeriodMethodResults<Period>)> for SVGGroup {
    fn from((method, results): (&String, PeriodMethodResults<Period>)) -> Self {
        let svg_result = SVGPeriodGroup::new(format!("p1.{}(p2)", method), results.result)
            .0
            .set("transform", svg_translate(&0, &(LINE_HEIGHT)));

        let group = SVGGroup::from((results.period1, results.period2))
            .group
            .add(svg_result);

        SVGGroup {
            group,
            line_count: 2,
        }
    }
}

impl From<(&String, PeriodMethodResults<Option<Period>>)> for SVGGroup {
    fn from((method, results): (&String, PeriodMethodResults<Option<Period>>)) -> Self {
        let svg_result = match results.result {
            Some(result) => SVGPeriodGroup::new(format!("p1.{}(p2)", method), result).0,
            None => Group::new().add(
                SVGText::from(format!("p1.{}(p2) = None", method))
                    .0
                    .set("class", "is-false")
                    .set("transform", svg_translate(&0, &(LINE_HEIGHT))),
            ),
        }
        .set("transform", svg_translate(&0, &(LINE_HEIGHT)));

        let group = SVGGroup::from((results.period1, results.period2))
            .group
            .add(svg_result);

        SVGGroup {
            group,
            line_count: 2,
        }
    }
}
