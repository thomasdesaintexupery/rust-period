use super::svg_translate;
use super::SVGGroup;
use super::SVGTest;
use super::SVGText;
use super::GAP;
use super::LINE_HEIGHT;
use crate::references::ranges::RangeMethodResults;
use crate::references::ranges::RangeMethodTest;
use crate::svg::period::SVGPeriodGroup;
use svg::node::element::Group;
use time_period::Range;

impl From<RangeMethodTest> for SVGTest {
    fn from(method_test: RangeMethodTest) -> Self {
        let svg_method_test = SVGTest::new(&method_test.title, super::SVGTestObject::Range)
            .add(SVGGroup::from((&method_test.title, method_test.out)))
            .add(SVGGroup::from((&method_test.title, method_test.next)))
            .add(SVGGroup::from((&method_test.title, method_test.encroach)))
            .add(SVGGroup::from((&method_test.title, method_test.inner)))
            .add(SVGGroup::from((&method_test.title, method_test.same)))
            .add(SVGGroup::from((&method_test.title, method_test.empty)))
            .add(SVGGroup::from((&method_test.title, method_test.bridge)))
            .add(SVGGroup::from((&method_test.title, method_test.overflow)));
        return svg_method_test;
    }
}

impl From<(&String, RangeMethodResults)> for SVGGroup {
    fn from((_, results): (&String, RangeMethodResults)) -> Self {
        SVGGroup {
            group: Group::new()
                .add(SVGRangeGroup::new("self".to_string(), results.base).0)
                .add(
                    SVGRangeGroup::new("range".to_string(), results.arg)
                        .0
                        .set("transform", svg_translate(&0, &(LINE_HEIGHT))),
                )
                .add(
                    SVGRangeGroup::new("->".to_string(), results.result)
                        .0
                        .set("transform", svg_translate(&0, &(LINE_HEIGHT * 2))),
                ),
            line_count: 3,
        }
    }
}

struct SVGRangeGroup(Group);

impl SVGRangeGroup {
    fn new(name: String, range: Range) -> Self {
        let mut group = Group::new();
        if range.len() == 0 {
            return Self(
                Group::new().add(
                    SVGText::from(format!("{} (empty)", name))
                        .0
                        .set("class", "range range-empty")
                        .set("y", LINE_HEIGHT / 2 + GAP)
                        .set("x", 0),
                ),
            );
        }

        group = group.add(
            SVGText::from(format!("{}", name))
                .0
                .set("class", "range range-empty")
                .set("y", LINE_HEIGHT / 2 + GAP)
                .set("x", 0),
        );

        let mut i = 1;
        for period in range.periods() {
            group = group.add(SVGPeriodGroup::new(format!("{}", i), *period).0);
            i = i + 1
        }

        Self(group.set("class", format!("range range-{}", name)))
    }
}
