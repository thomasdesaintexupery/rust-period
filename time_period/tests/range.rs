pub mod references;
use references::periods;
use time_period::Range;

#[test]
fn create_range() {
    assert_eq!(format!("{}", Range::new()), "{}");

    let mut range = Range::new();
    range = range.insert(&periods::before());
    range = range.insert(&periods::reference());

    assert_eq!(
            format!("{}", range),
            "{[2023-01-01T12:00:00+00:00,2023-01-23T12:00:00+00:00);[2023-03-09T12:00:00+00:00,2023-05-17T12:00:00+00:00)}"
    )
}

#[test]
fn equals() {
    assert_eq!(Range::new(), Range::new());

    let mut range = Range::new();
    range = range.insert(&periods::before());
    range = range.insert(&periods::reference());

    let range2 = range.clone();

    assert_eq!(range, range2);

    let mut range2 = Range::new();
    range2 = range2.insert(&periods::before());
    range2 = range2.insert(&periods::reference());

    assert_eq!(range, range2);
}

#[test]
fn insert_all() {
    let range = Range::new().insert_all(vec![periods::reference(), periods::head()].iter());

    let mut range2 = Range::new();
    range2 = range2.insert(&periods::reference());
    range2 = range2.insert(&periods::head());

    assert_eq!(range, range2)
}
