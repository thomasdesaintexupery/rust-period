pub mod references;
use references::{dates, periods};
use time_period::Period;

#[test]
fn union() {
    assert!(periods::reference().union(&periods::before()).is_none());
    assert!(periods::before().union(&periods::reference()).is_none());

    let union_head_ref =
        Period::new(periods::head().start, periods::reference().end).expect("Exist");
    assert_eq!(
        periods::reference().union(&periods::head()).unwrap(),
        union_head_ref
    );
    assert_eq!(
        periods::head().union(&periods::reference()).unwrap(),
        union_head_ref
    );

    let union_start_ref =
        Period::new(periods::starting().start, periods::reference().end).expect("Exist");
    assert_eq!(
        periods::reference().union(&periods::starting()).unwrap(),
        union_start_ref
    );
    assert_eq!(
        periods::starting().union(&periods::reference()).unwrap(),
        union_start_ref
    );

    assert_eq!(
        periods::reference().union(&periods::reference()).unwrap(),
        periods::reference()
    );
    assert_eq!(
        periods::reference().union(&periods::included()).unwrap(),
        periods::reference()
    );
    assert_eq!(
        periods::included().union(&periods::reference()).unwrap(),
        periods::reference()
    );
}

#[test]
fn maximize() {
    let max_start_ref = Period::new(dates::d01(), dates::d07()).expect("Exist");
    assert_eq!(
        periods::reference().maximize(&periods::before()),
        max_start_ref
    );
    assert_eq!(
        periods::before().maximize(&periods::reference()),
        max_start_ref
    );

    let max_head_ref = Period::new(dates::d02(), dates::d07()).expect("Exist");
    assert_eq!(
        periods::reference().maximize(&periods::head()),
        max_head_ref
    );
    assert_eq!(
        periods::head().maximize(&periods::reference()),
        max_head_ref
    );

    let max_starting_ref = Period::new(dates::d03(), dates::d07()).expect("Exist");
    assert_eq!(
        periods::reference().maximize(&periods::starting()),
        max_starting_ref
    );
    assert_eq!(
        periods::starting().maximize(&periods::reference()),
        max_starting_ref
    );

    assert_eq!(
        periods::reference().maximize(&periods::reference()),
        periods::reference()
    );

    assert_eq!(
        periods::reference().maximize(&periods::included()),
        periods::reference()
    );
    assert_eq!(
        periods::included().maximize(&periods::reference()),
        periods::reference()
    );
}

#[test]
fn between() {
    let between_before_and_ref = Period::new(dates::d02(), dates::d04()).unwrap();
    assert_eq!(
        periods::reference().between(&periods::before()).unwrap(),
        between_before_and_ref
    );
    assert_eq!(
        periods::before().between(&periods::reference()).unwrap(),
        between_before_and_ref
    );

    assert!(periods::reference().between(&periods::head()).is_none());
    assert!(periods::head().between(&periods::reference()).is_none());
    assert!(periods::reference().between(&periods::starting()).is_none());
    assert!(periods::starting().between(&periods::reference()).is_none());
    assert!(periods::reference()
        .between(&periods::reference())
        .is_none());
    assert!(periods::reference().between(&periods::included()).is_none());
    assert!(periods::included().between(&periods::reference()).is_none());
}

#[test]
fn intersect() {
    assert!(periods::before().intersect(&periods::head()).is_none());
    assert!(periods::reference().intersect(&periods::before()).is_none());

    assert!(periods::head().intersect(&periods::reference()).is_none());
    assert!(periods::reference().intersect(&periods::head()).is_none());

    let intersect_starting_and_ref = Period::new(dates::d04(), dates::d05()).unwrap();
    assert_eq!(
        periods::starting()
            .intersect(&periods::reference())
            .unwrap(),
        intersect_starting_and_ref
    );
    assert_eq!(
        periods::reference()
            .intersect(&periods::starting())
            .unwrap(),
        intersect_starting_and_ref
    );

    assert_eq!(
        periods::reference()
            .intersect(&periods::reference())
            .unwrap(),
        periods::reference()
    );

    assert_eq!(
        periods::included()
            .intersect(&periods::reference())
            .unwrap(),
        periods::included()
    );
    assert_eq!(
        periods::reference()
            .intersect(&periods::included())
            .unwrap(),
        periods::included()
    );
}
