pub mod references;
use references::dates::*;
use references::periods;
use references::ranges;
use references::ranges::RangeMethodTest;
use time_period::{Period, Range};

#[test]
fn merge() {
    let test = RangeMethodTest::new("union", Range::union);

    assert_eq!(
        test.out.result,
        Range::new()
            .insert(&periods::reference())
            .insert(&periods::p1013())
            .insert(&periods::p0102())
            .insert(&periods::p0809())
            .insert(&periods::p1415())
    );

    assert_eq!(
        test.next.result,
        Range::new().insert(&Period::new(d02(), d15()).unwrap())
    );

    assert_eq!(
        test.encroach.result,
        Range::new()
            .insert(&Period::new(d03(), d08()).unwrap())
            .insert(&Period::new(d09(), d14()).unwrap())
    );

    assert_eq!(test.inner.result, ranges::reference());

    assert_eq!(test.same.result, ranges::reference());

    assert_eq!(test.empty.result, ranges::reference());

    assert_eq!(
        test.bridge.result,
        Range::new().insert(&Period::new(d04(), d13()).unwrap())
    );

    assert_eq!(test.overflow.result, ranges::overflow());
}

#[test]
fn difference() {
    let test = RangeMethodTest::new("difference", Range::difference);

    assert_eq!(test.out.result, ranges::reference());

    assert_eq!(test.next.result, ranges::reference());

    assert_eq!(
        test.encroach.result,
        Range::new()
            .insert(&periods::p0506())
            .insert(&periods::p1112())
    );

    assert_eq!(
        test.inner.result,
        Range::new()
            .insert(&Period::new(d04(), d05()).unwrap())
            .insert(&Period::new(d06(), d07()).unwrap())
            .insert(&Period::new(d10(), d11()).unwrap())
            .insert(&Period::new(d12(), d13()).unwrap())
    );

    assert_eq!(test.same.result, Range::new());

    assert_eq!(test.empty.result, ranges::reference());

    assert_eq!(
        test.bridge.result,
        Range::new()
            .insert(&Period::new(d04(), d06()).unwrap())
            .insert(&Period::new(d11(), d13()).unwrap())
    );

    assert_eq!(test.overflow.result, Range::new());
}

#[test]
fn exclusion() {
    let test = RangeMethodTest::new("exclusion", Range::exclusion);

    assert_eq!(
        test.out.result,
        Range::new()
            .insert(&periods::reference())
            .insert(&periods::p1013())
            .insert(&periods::p0102())
            .insert(&periods::p0809())
            .insert(&periods::p1415())
    );

    assert_eq!(
        test.next.result,
        Range::new().insert(&Period::new(d02(), d15()).unwrap())
    );

    assert_eq!(
        test.encroach.result,
        Range::new()
            .insert(&Period::new(d03(), d04()).unwrap())
            .insert(&Period::new(d05(), d06()).unwrap())
            .insert(&Period::new(d07(), d08()).unwrap())
            .insert(&Period::new(d09(), d10()).unwrap())
            .insert(&Period::new(d11(), d12()).unwrap())
            .insert(&Period::new(d13(), d14()).unwrap())
    );

    assert_eq!(
        test.inner.result,
        Range::new()
            .insert(&Period::new(d04(), d05()).unwrap())
            .insert(&Period::new(d06(), d07()).unwrap())
            .insert(&Period::new(d10(), d11()).unwrap())
            .insert(&Period::new(d12(), d13()).unwrap())
    );

    assert_eq!(test.same.result, Range::new());

    assert_eq!(test.empty.result, ranges::reference());

    assert_eq!(
        test.bridge.result,
        Range::new()
            .insert(&Period::new(d04(), d06()).unwrap())
            .insert(&Period::new(d07(), d10()).unwrap())
            .insert(&Period::new(d11(), d13()).unwrap())
    );

    assert_eq!(
        test.overflow.result,
        Range::new()
            .insert(&Period::new(d03(), d04()).unwrap())
            .insert(&Period::new(d07(), d10()).unwrap())
    );
}

#[test]
fn intersection() {
    let test = RangeMethodTest::new("intersection", Range::intersection);

    assert_eq!(test.out.result, Range::new());

    assert_eq!(test.next.result, Range::new());

    assert_eq!(
        test.encroach.result,
        Range::new()
            .insert(&Period::new(d04(), d05()).unwrap())
            .insert(&Period::new(d06(), d07()).unwrap())
            .insert(&Period::new(d10(), d11()).unwrap())
            .insert(&Period::new(d12(), d13()).unwrap())
    );

    assert_eq!(
        test.inner.result,
        Range::new()
            .insert(&periods::p0506())
            .insert(&periods::p1112())
    );

    assert_eq!(test.same.result, ranges::reference());

    assert_eq!(test.empty.result, Range::new());

    assert_eq!(
        test.bridge.result,
        Range::new()
            .insert(&Period::new(d06(), d07()).unwrap())
            .insert(&Period::new(d10(), d11()).unwrap())
    );

    assert_eq!(test.overflow.result, ranges::reference());
}
