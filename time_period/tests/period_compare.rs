pub mod references;
use references::periods;

#[test]
fn is_strictly_before() {
    assert_eq!(
        periods::reference().is_strictly_before(&periods::before()),
        false
    );
    assert_eq!(
        periods::before().is_strictly_before(&periods::reference()),
        true
    );

    assert_eq!(
        periods::reference().is_strictly_before(&periods::head()),
        false
    );
    assert_eq!(
        periods::head().is_strictly_before(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_before(&periods::starting()),
        false
    );
    assert_eq!(
        periods::starting().is_strictly_before(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_before(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_before(&periods::included()),
        false
    );
    assert_eq!(
        periods::included().is_strictly_before(&periods::reference()),
        false
    );
}

#[test]
fn is_before() {
    assert_eq!(periods::reference().is_before(&periods::before()), false);
    assert_eq!(periods::before().is_before(&periods::reference()), true);

    assert_eq!(periods::reference().is_before(&periods::head()), false);
    assert_eq!(periods::head().is_before(&periods::reference()), true);

    assert_eq!(periods::reference().is_before(&periods::starting()), false);
    assert_eq!(periods::starting().is_before(&periods::reference()), false);

    assert_eq!(periods::reference().is_before(&periods::reference()), false);

    assert_eq!(periods::reference().is_before(&periods::included()), false);
    assert_eq!(periods::included().is_before(&periods::reference()), false);
}

#[test]
fn is_heading() {
    assert_eq!(periods::reference().is_heading(&periods::before()), false);
    assert_eq!(periods::before().is_heading(&periods::reference()), false);

    assert_eq!(periods::reference().is_heading(&periods::head()), false);
    assert_eq!(periods::head().is_heading(&periods::reference()), true);

    assert_eq!(periods::reference().is_heading(&periods::starting()), false);
    assert_eq!(periods::starting().is_heading(&periods::reference()), false);

    assert_eq!(
        periods::reference().is_heading(&periods::reference()),
        false
    );

    assert_eq!(periods::reference().is_heading(&periods::included()), false);
    assert_eq!(periods::included().is_heading(&periods::reference()), false);
}

#[test]
fn is_included() {
    assert_eq!(periods::reference().is_included(&periods::before()), false);
    assert_eq!(periods::before().is_included(&periods::reference()), false);

    assert_eq!(periods::reference().is_included(&periods::head()), false);
    assert_eq!(periods::head().is_included(&periods::reference()), false);

    assert_eq!(
        periods::reference().is_included(&periods::starting()),
        false
    );
    assert_eq!(
        periods::starting().is_included(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_included(&periods::reference()),
        true
    );

    assert_eq!(
        periods::reference().is_included(&periods::included()),
        false
    );
    assert_eq!(periods::included().is_included(&periods::reference()), true);
}

#[test]
fn is_tailing() {
    assert_eq!(periods::reference().is_tailing(&periods::before()), false);
    assert_eq!(periods::before().is_tailing(&periods::reference()), false);

    assert_eq!(periods::reference().is_tailing(&periods::head()), true);
    assert_eq!(periods::head().is_tailing(&periods::reference()), false);

    assert_eq!(periods::reference().is_tailing(&periods::starting()), false);
    assert_eq!(periods::starting().is_tailing(&periods::reference()), false);

    assert_eq!(
        periods::reference().is_tailing(&periods::reference()),
        false
    );

    assert_eq!(periods::reference().is_tailing(&periods::included()), false);
    assert_eq!(periods::included().is_tailing(&periods::reference()), false);
}

#[test]
fn is_after() {
    assert_eq!(periods::reference().is_after(&periods::before()), true);
    assert_eq!(periods::before().is_after(&periods::reference()), false);

    assert_eq!(periods::reference().is_after(&periods::head()), true);
    assert_eq!(periods::head().is_after(&periods::reference()), false);

    assert_eq!(periods::reference().is_after(&periods::starting()), false);
    assert_eq!(periods::starting().is_after(&periods::reference()), false);

    assert_eq!(periods::reference().is_after(&periods::reference()), false);

    assert_eq!(periods::reference().is_after(&periods::included()), false);
    assert_eq!(periods::included().is_after(&periods::reference()), false);
}

#[test]
fn is_strictly_after() {
    assert_eq!(
        periods::reference().is_strictly_after(&periods::before()),
        true
    );
    assert_eq!(
        periods::before().is_strictly_after(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_after(&periods::head()),
        false
    );
    assert_eq!(
        periods::head().is_strictly_after(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_after(&periods::starting()),
        false
    );
    assert_eq!(
        periods::starting().is_strictly_after(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_after(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_strictly_after(&periods::included()),
        false
    );
    assert_eq!(
        periods::included().is_strictly_after(&periods::reference()),
        false
    );
}

#[test]
fn is_intersecting() {
    assert_eq!(
        periods::reference().is_intersecting(&periods::before()),
        false
    );
    assert_eq!(
        periods::before().is_intersecting(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_intersecting(&periods::head()),
        false
    );
    assert_eq!(
        periods::head().is_intersecting(&periods::reference()),
        false
    );

    assert_eq!(
        periods::reference().is_intersecting(&periods::starting()),
        true
    );
    assert_eq!(
        periods::starting().is_intersecting(&periods::reference()),
        true
    );

    assert_eq!(
        periods::reference().is_intersecting(&periods::reference()),
        true
    );

    assert_eq!(
        periods::reference().is_intersecting(&periods::included()),
        true
    );
    assert_eq!(
        periods::included().is_intersecting(&periods::reference()),
        true
    );
}
