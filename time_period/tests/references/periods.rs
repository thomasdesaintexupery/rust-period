use time_period::Period;

use super::dates::*;

pub fn p0102() -> Period {
    Period::new(d01(), d02()).expect("p0102 period sould exist")
}

/// [Period] before [REF] with a gap of time
pub fn before() -> Period {
    p0102()
}

pub fn p0204() -> Period {
    Period::new(d02(), d04()).expect("p0204 period sould exist")
}

/// [Period] right before [reference]
pub fn head() -> Period {
    p0204()
}

pub fn p0305() -> Period {
    Period::new(d03(), d05()).expect("p0305 period sould exist")
}

/// [Period] starting before [REF] and ending within
pub fn starting() -> Period {
    p0305()
}

pub fn p0407() -> Period {
    Period::new(d04(), d07()).expect("p0407 period sould exist")
}

/// Test reference [Period]
pub fn reference() -> Period {
    p0407()
}

pub fn p0506() -> Period {
    Period::new(d05(), d06()).expect("p0506 period sould exist")
}

/// [Period] completely included in [REF]
pub fn included() -> Period {
    p0506()
}

pub fn enclose() -> Period {
    p0308()
}

pub fn ending() -> Period {
    p0608()
}

pub fn tail() -> Period {
    p0708()
}

pub fn after() -> Period {
    p0809()
}

pub fn p0308() -> Period {
    Period::new(d03(), d08()).expect("p0308 should exists")
}
pub fn p0608() -> Period {
    Period::new(d06(), d08()).expect("p0608 should exists")
}
pub fn p0610() -> Period {
    Period::new(d06(), d10()).expect("p0610 should exists")
}
pub fn p0611() -> Period {
    Period::new(d06(), d11()).expect("p0611 should exists")
}
pub fn p0708() -> Period {
    Period::new(d07(), d09()).expect("p0709 should exists")
}
pub fn p0710() -> Period {
    Period::new(d07(), d10()).expect("p0710 should exists")
}
pub fn p0711() -> Period {
    Period::new(d07(), d11()).expect("p0711 should exists")
}
pub fn p0809() -> Period {
    Period::new(d08(), d09()).expect("p0809 should exists")
}
pub fn p0910() -> Period {
    Period::new(d09(), d10()).expect("p0910 should exists")
}
pub fn p0911() -> Period {
    Period::new(d09(), d11()).expect("p0911 should exists")
}
pub fn p1013() -> Period {
    Period::new(d10(), d13()).expect("p1013 should exists")
}
pub fn p1112() -> Period {
    Period::new(d11(), d12()).expect("p1112 should exists")
}
pub fn p1214() -> Period {
    Period::new(d12(), d14()).expect("p1214 should exists")
}
pub fn p0313() -> Period {
    Period::new(d03(), d13()).expect("p0313 should exists")
}
pub fn p1315() -> Period {
    Period::new(d13(), d15()).expect("p1315 should exists")
}
pub fn p1415() -> Period {
    Period::new(d14(), d15()).expect("p1415 should exists")
}

type PeriodMethod<ResultType> = fn(period1: &Period, period2: &Period) -> ResultType;

pub struct PeriodMethodTest<ResultType> {
    pub title: String,
    pub before: PeriodMethodResults<ResultType>,
    pub head: PeriodMethodResults<ResultType>,
    pub starting: PeriodMethodResults<ResultType>,
    pub same: PeriodMethodResults<ResultType>,
    pub included: PeriodMethodResults<ResultType>,
    pub enclose: PeriodMethodResults<ResultType>,
    pub tail: PeriodMethodResults<ResultType>,
    pub ending: PeriodMethodResults<ResultType>,
    pub after: PeriodMethodResults<ResultType>,
}

impl<ResultType> PeriodMethodTest<ResultType> {
    pub fn new(title: &str, method: PeriodMethod<ResultType>) -> Self {
        return Self {
            title: title.into(),
            before: PeriodMethodResults::new(&before(), method),
            head: PeriodMethodResults::new(&head(), method),
            starting: PeriodMethodResults::new(&starting(), method),
            same: PeriodMethodResults::new(&reference(), method),
            included: PeriodMethodResults::new(&included(), method),
            enclose: PeriodMethodResults::new(&enclose(), method),
            tail: PeriodMethodResults::new(&tail(), method),
            ending: PeriodMethodResults::new(&ending(), method),
            after: PeriodMethodResults::new(&after(), method),
        };
    }
}

pub struct PeriodMethodResults<ResultType> {
    pub period1: Period,
    pub period2: Period,
    pub result: ResultType,
}

impl<ResultType> PeriodMethodResults<ResultType> {
    pub fn new(period: &Period, method: PeriodMethod<ResultType>) -> Self {
        let period1 = period.clone();
        let period2 = reference().clone();

        return Self {
            period1,
            period2,
            result: method(&period1, &period2),
        };
    }
}
