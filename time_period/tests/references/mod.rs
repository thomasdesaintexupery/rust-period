//! # Period test dataset
//!
//! * REF is the reference period
//! * BEFORE is before REF with a gap of time
//! * HEAD is right before REF
//! * STARTING starts before REF and end within
//! * INSIDE is completely included in REF
//!
//! ## Here a visual representation
//!
//! p0102 (BEFORE)   d1___d2
//! p0204 (HEAD)          d2________d4
//! p0305 (STARTING)           d3_________d5
//! p0407 (REFERENCE)               d4=============d7
//! p0506 (INCLUDED)                      d5___d6
//! p0308 (ENCLOSE)            d3_________________________d8
//! p0608 (ENDING)                             d6_________d8
//! p0610                                      d6______________________d10
//! p0611                                      d6___________________________d11
//! p0708 (TAIL)                                   d7_____d8
//! p0710                                          d7__________________d10
//! p0711                                          d7_______________________d11
//! p0809 (AFTER BETWEEN)                                 d8___d9
//! p0910                                                      d9______d10
//! p0911                                                      d9___________d11
//! p1013 (REFERENCE 2)                                                d10=============d13
//! p1112                                                                   d11___d12
//! p1214                                                                         d12_______d14
//! p0313                       d3_____________________________________________________d13
//! p1315                                                                              d13_________d15
//! p1415 (LAST)                                                                             d14___d15
pub mod dates;
pub mod periods;
pub mod ranges;
