//! p0102 (BEFORE)   d1___d2
//! p0204 (HEAD)          d2________d4
//! p0305 (STARTING)           d3_________d5
//! p0407 (REFERENCE)               d4=============d7
//! p0506 (INCLUDED)                      d5___d6
//! p0308 (ENCLOSE)            d3_________________________d8
//! p0608 (ENDING)                             d6_________d8
//! p0610                                      d6______________________d10
//! p0611                                      d6___________________________d11
//! p0708 (TAIL)                                   d7_____d8
//! p0710                                          d7__________________d10
//! p0711                                          d7_______________________d11
//! p0809 (AFTER BETWEEN)                                 d8___d9
//! p0910                                                      d9______d10
//! p0911                                                      d9___________d11
//! p1013 (REFERENCE 2)                                                d10=============d13
//! p1112                                                                   d11___d12
//! p1214                                                                         d12_______d14
//! p0315                       d3_____________________________________________________d13
//! p1315                                                                              d13_________d15
//! p1415 (LAST)                                                                             d14___d15
//!
//! reference                       d4=============d7                  d10=============d13
//! out              d1___d2                              d8___d9                            d14___d15
//! next                  d2________d4             d7_____d8   d9______d10             d13_________d15
//! encroach                   d3_________d5   d6_________d8   d9___________d11   d12_______d14
//! inner                                 d5___d6                           d11___d12
//! bridge                                     d6___________________________d11
//! overflow                   d3_____________________________________________________d13
use time_period::Range;

use super::periods;

pub fn reference() -> Range {
    Range::new()
        .insert(&periods::reference())
        .insert(&periods::p1013())
}

pub fn out() -> Range {
    Range::new()
        .insert(&periods::p0102())
        .insert(&periods::p0809())
        .insert(&periods::p1415())
}

pub fn next() -> Range {
    Range::new()
        .insert(&periods::p0204())
        .insert(&periods::p0708())
        .insert(&periods::p0910())
        .insert(&periods::p1315())
}

pub fn encroach() -> Range {
    Range::new()
        .insert(&periods::p0305())
        .insert(&periods::p0608())
        .insert(&periods::p0911())
        .insert(&periods::p1214())
}

pub fn inner() -> Range {
    Range::new()
        .insert(&periods::p0506())
        .insert(&periods::p1112())
}

pub fn merge() -> Range {
    Range::new().insert(&periods::p0611())
}

pub fn overflow() -> Range {
    Range::new().insert(&periods::p0313())
}

type RangeMethod = fn(range1: Range, range3: &Range) -> Range;

pub struct RangeMethodResults {
    pub base: Range,
    pub arg: Range,
    pub result: Range,
}

impl RangeMethodResults {
    pub fn new(range: Range, method: RangeMethod) -> Self {
        let ref_range = reference();
        return Self {
            base: ref_range.clone(),
            arg: range.clone(),
            result: method(ref_range, &range),
        };
    }
}

pub struct RangeMethodTest {
    pub title: String,
    pub out: RangeMethodResults,
    pub next: RangeMethodResults,
    pub encroach: RangeMethodResults,
    pub inner: RangeMethodResults,
    pub same: RangeMethodResults,
    pub empty: RangeMethodResults,
    pub bridge: RangeMethodResults,
    pub overflow: RangeMethodResults,
}

impl RangeMethodTest {
    pub fn new(title: &str, method: RangeMethod) -> Self {
        return Self {
            title: title.into(),
            out: RangeMethodResults::new(out(), method),
            next: RangeMethodResults::new(next(), method),
            encroach: RangeMethodResults::new(encroach(), method),
            inner: RangeMethodResults::new(inner(), method),
            same: RangeMethodResults::new(reference(), method),
            empty: RangeMethodResults::new(Range::new(), method),
            bridge: RangeMethodResults::new(merge(), method),
            overflow: RangeMethodResults::new(overflow(), method),
        };
    }
}
