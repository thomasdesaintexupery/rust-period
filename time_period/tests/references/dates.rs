use chrono::prelude::*;
use time_period::PDateTime;

pub fn d01() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 01, 01, 12, 0, 0).unwrap()
}
pub fn d02() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 01, 23, 12, 0, 0).unwrap()
}

pub fn d03() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 02, 07, 12, 0, 0).unwrap()
}

/// Start [DateTime] for the main reference period
pub fn d04() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 03, 09, 12, 0, 0).unwrap()
}

pub fn start() -> PDateTime {
    d04()
}

pub fn d05() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 03, 17, 12, 0, 0).unwrap()
}

pub fn d06() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 04, 29, 12, 0, 0).unwrap()
}

/// End [DateTime] for the main reference period
pub fn d07() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 05, 17, 12, 0, 0).unwrap()
}
pub fn end() -> PDateTime {
    d07()
}

pub fn d08() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 06, 05, 12, 0, 0).unwrap()
}

pub fn d09() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 06, 23, 12, 0, 0).unwrap()
}

pub fn d10() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 07, 15, 12, 0, 0).unwrap()
}

pub fn d11() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 07, 19, 12, 0, 0).unwrap()
}

pub fn d12() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 07, 25, 12, 0, 0).unwrap()
}

pub fn d13() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 07, 29, 12, 0, 0).unwrap()
}

pub fn d14() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 08, 03, 12, 0, 0).unwrap()
}

pub fn d15() -> PDateTime {
    Utc.with_ymd_and_hms(2023, 08, 07, 12, 0, 0).unwrap()
}
