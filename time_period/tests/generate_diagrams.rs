pub mod references;
use references::periods::PeriodMethodTest;
use references::ranges::RangeMethodTest;

use time_period::{Period, Range};

pub mod svg;
use crate::svg::{recreate_period_dir, recreate_range_dir, SVGTest};

#[test]
fn generate_for_periods() {
    recreate_period_dir().expect("to work");

    SVGTest::from(PeriodMethodTest::new("is_before", Period::is_before)).write();
    SVGTest::from(PeriodMethodTest::new(
        "is_strictly_before",
        Period::is_strictly_before,
    ))
    .write();
    SVGTest::from(PeriodMethodTest::new("is_heading", Period::is_heading)).write();
    SVGTest::from(PeriodMethodTest::new("is_after", Period::is_after)).write();
    SVGTest::from(PeriodMethodTest::new(
        "is_strictly_after",
        Period::is_strictly_after,
    ))
    .write();
    SVGTest::from(PeriodMethodTest::new("is_tailing", Period::is_tailing)).write();
    SVGTest::from(PeriodMethodTest::new("is_included", Period::is_included)).write();
    SVGTest::from(PeriodMethodTest::new(
        "is_intersecting",
        Period::is_intersecting,
    ))
    .write();
    SVGTest::from(PeriodMethodTest::new("union", Period::union)).write();
    SVGTest::from(PeriodMethodTest::new("between", Period::between)).write();
    SVGTest::from(PeriodMethodTest::new("maximize", Period::maximize)).write();
    SVGTest::from(PeriodMethodTest::new("intersect", Period::intersect)).write();
}

#[test]

fn generate_for_ranges() {
    recreate_range_dir().expect("to work");
    SVGTest::from(RangeMethodTest::new("union", Range::union)).write();
    SVGTest::from(RangeMethodTest::new("difference", Range::difference)).write();
    SVGTest::from(RangeMethodTest::new("exclusion", Range::exclusion)).write();
    SVGTest::from(RangeMethodTest::new("intersection", Range::intersection)).write();
}
