mod period;
mod range;
pub mod result;

pub use crate::period::{PDateTime, Period};
pub use crate::range::Range;
