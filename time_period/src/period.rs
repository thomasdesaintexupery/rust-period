use chrono::prelude::*;
use chrono::Duration;
use chrono::ParseResult;
use std::fmt;
use std::str::FromStr;

use crate::result;
pub use result::{PeriodError, PeriodResult, IMPOSSIBLE};

/// A [Period] represents a [left-closed, right-open bounded interval](https://en.wikipedia.org/wiki/Interval_(mathematics)) of time with two [PDateTime] as boundaries.
///
/// Any *dt* [PDateTime] is
/// * **included** if *start* ≤ *dt* < *end*,
/// * **excluded** if `dt < start or end ≤ dt`.
#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub struct Period {
    pub start: PDateTime,
    pub end: PDateTime,
}

/// A point in time in [Period]
pub type PDateTime = DateTime<Utc>;

impl Period {
    /// Creates a [Period] with 2 [PDateTime] boundaries.
    pub fn new<D>(start: D, end: D) -> Option<Self>
    where
        D: Into<PDateTime>,
    {
        let start = start.into();
        let end = end.into();
        if start < end {
            Some(Self { start, end })
        } else {
            None
        }
    }

    /// Creates a [Period] from 2 moment expressed as [str], it's recommended to use [rfc3339](https://www.rfc-editor.org/rfc/rfc3339) representation.
    pub fn from_iso<S>(start: S, end: S) -> ParseResult<Option<Self>>
    where
        S: Into<String>,
    {
        let start: PDateTime = PDateTime::from_str(&start.into()[..])?;
        let end: PDateTime = PDateTime::from_str(&end.into()[..])?;

        Ok(Period::new(start, end))
    }

    /// Returns the [Duration] of the [Period].
    pub fn duration(&self) -> Duration {
        self.end.signed_duration_since(self.start)
    }

    /// Returns true if given [PDateTime] is inside self.
    pub fn contains(&self, datetime: &PDateTime) -> bool {
        &self.start <= datetime && &self.end > datetime
    }

    /// Return true if self ends before the start if given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_strictly_before.svg")]
    /// </details>
    pub fn is_strictly_before(&self, period: &Self) -> bool {
        self.end < period.start
    }

    /// Returns true if self ends before or at the start of given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_before.svg")]
    /// </details>
    pub fn is_before(&self, period: &Self) -> bool {
        self.end <= period.start
    }

    /// Returns true if self ends right at the start of given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_heading.svg")]
    /// </details>
    pub fn is_heading(&self, period: &Self) -> bool {
        self.end == period.start
    }

    /// Returns true if self is included given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_included.svg")]
    /// </details>
    pub fn is_included(&self, period: &Self) -> bool {
        self.start >= period.start && self.end <= period.end
    }

    /// Returns true if self ends right at the end of given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_tailing.svg")]
    /// </details>
    pub fn is_tailing(&self, period: &Self) -> bool {
        self.start == period.end
    }

    /// Returns true if self starts at or after the end of given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_after.svg")]
    /// </details>
    pub fn is_after(&self, period: &Self) -> bool {
        self.start >= period.end
    }

    /// Returns true if self starts after the end of given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_strictly_after.svg")]
    /// </details>
    pub fn is_strictly_after(&self, period: &Self) -> bool {
        self.start > period.end
    }

    /// Returns true if self intersect with given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/is_intersecting.svg")]
    /// </details>
    pub fn is_intersecting(&self, period: &Self) -> bool {
        !self.is_before(period) && !self.is_after(period)
    }

    /// Creates a new [Period] representing the union of self and given [Period].
    /// ⚠️ It returns [None] if there is a gap between period
    /// See [`Period::maximize`]
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/union.svg")]
    /// </details>
    pub fn union(&self, period: &Self) -> Option<Period> {
        if self.is_strictly_before(period) || self.is_strictly_after(period) {
            return None;
        }

        return Some(self.maximize(period));
    }

    /// Creates the smallest [Period] including both self and given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/maximize.svg")]
    /// </details>
    pub fn maximize(&self, period: &Self) -> Period {
        Period {
            start: DateTime::min(self.start, period.start),
            end: DateTime::max(self.end, period.end),
        }
    }

    /// Creates the widest [Period] bounded by self and given [Period].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/between.svg")]
    /// </details>
    pub fn between(&self, period: &Self) -> Option<Period> {
        if self.is_strictly_before(period) {
            return Some(Period {
                start: self.end,
                end: period.start,
            });
        } else if self.is_strictly_after(period) {
            return Some(Period {
                start: period.end,
                end: self.start,
            });
        } else {
            return None;
        };
    }

    /// Creates a new [Period] representing the intersection of self and given [Period].
    /// ⚠️ It returns [None] if there is no intersection
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/period/intersect.svg")]
    /// </details>
    pub fn intersect(&self, period: &Self) -> Option<Period> {
        if self.is_before(period) || self.is_after(period) {
            return None;
        }

        Some(Period {
            start: DateTime::max(self.start, period.start),
            end: DateTime::min(self.end, period.end),
        })
    }
}

impl fmt::Display for Period {
    // Let's use [Math right-open interval notation](https://en.wikipedia.org/wiki/Interval_(mathematics)) to represent a Period
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{},{})", self.start.to_rfc3339(), self.end.to_rfc3339())
    }
}
