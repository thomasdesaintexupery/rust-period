use core::fmt;

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub struct PeriodError(PeriodErrorKind);

impl PeriodError {
    pub fn kind(&self) -> PeriodErrorKind {
        self.0
    }
}

/// The category of parse error
#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum PeriodErrorKind {
    // Would result in an impossible period of time
    Impossible,

    // TODO: Change this to `#[non_exhaustive]` (on the enum) when MSRV is increased
    #[doc(hidden)]
    __Nonexhaustive,
}

pub static IMPOSSIBLE: PeriodError = PeriodError(PeriodErrorKind::Impossible);

impl fmt::Display for PeriodError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.0 {
            PeriodErrorKind::Impossible => write!(f, "no possible period"),
            _ => unreachable!(),
        }
    }
}

/// Same as `Result<T, ParseError>`.
pub type PeriodResult<T> = Result<T, PeriodError>;
