use std::{
    collections::{btree_map::Values, BTreeMap},
    fmt,
    ops::Bound,
};

use crate::{PDateTime, Period};

/// A [Range] is an aggregate of ordered [Period]
///
/// For any consecutive periods p1 and p2, p1.before(p2) must returns true)
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Range {
    /// Storted [Period] by start
    periods: BTreeMap<PDateTime, Period>,
}

impl Range {
    pub fn new() -> Self {
        Self {
            periods: BTreeMap::new(),
        }
    }

    /// Consumes self, inserts given [Period] and return self
    ///
    /// See [`Range::union`] examples
    pub fn insert(mut self, period: &Period) -> Self {
        if self.periods.is_empty() {
            self.periods.insert(period.start, period.clone());
            return self;
        }

        let mut range = self
            .periods
            .range_mut((Bound::Included(&period.start), Bound::Included(&period.end)));

        let mut to_remove: Vec<PDateTime> = vec![];
        let mut period_to_insert = period.clone();
        while let Some((start, period_iter)) = range.next() {
            period_to_insert = period_to_insert.maximize(period_iter);
            to_remove.push(start.clone());
        }

        match self
            .periods
            .range_mut((Bound::Unbounded, Bound::Excluded(&period_to_insert.start)))
            .last()
        {
            Some((previous_start, previous_period)) => {
                if previous_period.end >= period_to_insert.start {
                    period_to_insert = period_to_insert.maximize(previous_period);
                    to_remove.push(previous_start.clone());
                }
            }
            None => (),
        }

        for start_date in to_remove {
            self.periods.remove(&start_date);
        }

        self.periods
            .insert(period_to_insert.start, period_to_insert);

        self
    }

    /// Inserts all given periods
    ///
    /// See [`Range::union`] examples
    pub fn insert_all<'a, I>(mut self, periods: I) -> Self
    where
        I: Iterator<Item = &'a Period>,
    {
        for period in periods {
            self = self.insert(period);
        }

        return self;
    }

    /// Consumes self and return the union of given [Range] and self.
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/range/union.svg")]
    /// </details>
    pub fn union(self, range: &Range) -> Self {
        self.insert_all(range.periods.values())
    }

    /// Consumes self, remove given [Period] returns self
    ///
    /// See [`Range::difference`] examples
    pub fn remove(mut self, period: &Period) -> Self {
        if self.periods.is_empty() {
            return self;
        }

        let mut range = self
            .periods
            .range_mut((Bound::Included(&period.start), Bound::Included(&period.end)));

        let mut to_remove: Vec<PDateTime> = vec![];
        let mut to_insert: Vec<Period> = vec![];
        while let Some((start, period_iter)) = range.next() {
            to_remove.push(start.clone());

            if period_iter.end > period.end {
                to_insert.push(Period::new(period.end.clone(), period_iter.end.clone()).unwrap())
            }
        }

        match self
            .periods
            .range_mut((Bound::Unbounded, Bound::Excluded(&period.start)))
            .last()
        {
            Some((previous_start, previous_period)) => {
                if previous_period.end >= period.start {
                    to_insert.push(
                        Period::new(previous_period.start.clone(), period.start.clone()).unwrap(),
                    );
                    if previous_period.end > period.end {
                        to_insert.push(
                            Period::new(period.end.clone(), previous_period.end.clone()).unwrap(),
                        );
                    }
                    to_remove.push(previous_start.clone());
                }
            }
            None => (),
        }

        for start_date in to_remove {
            self.periods.remove(&start_date);
        }

        for period_to_insert in to_insert {
            self.periods
                .insert(period_to_insert.start, period_to_insert);
        }

        self
    }

    /// Removes all given periods
    ///
    /// See [`Range::difference`] examples
    pub fn remove_all<'a, I>(mut self, periods: I) -> Self
    where
        I: Iterator<Item = &'a Period>,
    {
        for period in periods {
            self = self.remove(period);
        }

        return self;
    }

    /// Consumes self and return self without given [Range].
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/range/difference.svg")]
    /// </details>
    pub fn difference(self, range: &Range) -> Self {
        self.remove_all(range.periods.values())
    }

    /// Gets an iterator over the [Period], in order by start [PDateTime].
    pub fn periods(&self) -> Values<'_, PDateTime, Period> {
        self.periods.values()
    }

    /// Returns the number of [Period].
    pub fn len(&self) -> usize {
        self.periods.len()
    }

    /// Consumes self, returns [Range] with only periods present once (self xor range)
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/range/exclusion.svg")]
    /// </details>
    pub fn exclusion(self, range: &Range) -> Self {
        let range_part1 = &range.clone().difference(&self);
        self.difference(range).union(range_part1)
    }

    /// Consumes self, returns [Range] with periods present in both [Range] (self && range)
    ///
    /// <details>
    /// <summary>Examples</summary>
    #[doc=include_str!("../diagrams/range/intersection.svg")]
    /// </details>
    pub fn intersection(self, range: &Range) -> Self {
        let exclusion = self.clone().exclusion(range);
        self.union(range).difference(&exclusion)
    }
}

impl From<Vec<Period>> for Range {
    fn from(value: Vec<Period>) -> Self {
        Range::new().insert_all(value.iter())
    }
}

impl fmt::Display for Range {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{{{}}}",
            self.periods
                .iter()
                .map(|(_, p)| p.to_string())
                .collect::<Vec<String>>()
                .join(";")
        )
    }
}
