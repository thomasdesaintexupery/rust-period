import { Period } from "js-period/js_period";

const date = new Date("2023-01-01T01:02:03+02:00");
const date2 = new Date("2023-01-02T01:02:03Z");

let period = new Period(date, date2);
console.log(period);

let period2 = new Period(date, date);
console.log(period2);
