# Rust Period

Goal : compute mathematical operation on Period (Interval of time) and Ranges (List of Period). This is done in Rust and package for typescript/js use via wasm-bindgen.

## Test

Run `make test` to run cargo tests and typescript integration tests.

## Build

Run `make build`
